# Errors
pyside6-not-found-error = The GUI requires PySide6, but it was not found on your system.
                          Portmod does not automatically install PySide6 or Qt.

## Main window
manage-tab-label = Manage
search-tab-label = Search

## Manage tab

# Packages filter
manage-filter-field-placeholder-text = Filter packages...
manage-filter-button-text = Filter

# Installed packages table
installed-packages-table-context-menu-properties = Properties
installed-packages-table-context-menu-disable = Disable
installed-packages-table-context-menu-uninstall = Uninstall
installed-packages-table-context-menu-rebuild = Rebuild

# Package details window
details-window-local-flags-title = Package specific use flags
details-window-local-flags-use-info-tooltip = Use flags can be used to control optional dependencies and optional configurations
details-panel-version-label = Version
details-panel-size-label = Size
details-panel-license-label = License
details-panel-homepage-button-label = Homepage
details-panel-details-tab-label = Details
details-panel-flags-tab-label = Flags

## Search tab

search-text-field-placeholder-text = Search packages...
search-button-text = Search
